package no.ntnu.idatt2001;

import no.ntnu.idatt2001.PatientApplication.MainWindowController;
import no.ntnu.idatt2001.PatientApplication.patients.Patient;
import no.ntnu.idatt2001.PatientApplication.patients.PatientRegister;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


/**
 * Denne klassen er ment for å teste GUI. Ser at jeg trolig må bruke TeastFX 3 eller 4 for å gjøre dette,
 * men pga tidstrøbbel får jeg ikke implementert dette. Prøver her å vise at jeg vet hvordan tester skal
 * settes opp og ville videre lagt inn testene for å se at programmet fungerer.
 */
public class MainWindowControllerTest {

    Patient patient = new Patient();
    MainWindowController mainWindowController = new MainWindowController();

    @BeforeEach
    public void setUp() {
        patient = new Patient("Aleksander", "Røed", "1234567890");
    }


}
