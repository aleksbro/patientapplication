package no.ntnu.idatt2001.PatientApplication;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import no.ntnu.idatt2001.PatientApplication.patients.Patient;
import no.ntnu.idatt2001.PatientApplication.patients.PatientRegister;


import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class MainWindowController implements Initializable {


    @FXML
    private ImageView addPatientButton;

    @FXML
    private MenuItem fileAddNewPatient;


    @FXML
    private TableView<Patient> tableView;
    @FXML
    private TableColumn<Patient, String> firstnameColumn;
    @FXML
    private TableColumn<Patient, String> lastNameColumn;
    @FXML
    private TableColumn<Patient, String> socialSecurityNumberColumn;

    @FXML
    private TextField firstNameTextField;
    @FXML
    private TextField lastNameTextField;
    @FXML
    private TextField socialSecurityNumberTextField;

    @FXML
    private Button addPatientPushButton;

    public MainWindowController() {

    }


    /**
     *
     * @param actionEvent
     *
     * Dette er metoden som gjør at man kan legge til en ny pasient i tableview.
     * Den tar inn ActionEvent som param og henter deretter teksten som er
     * skrevet i textfieldsene. Til slutt fjerner den teksten slik at man kan
     * skrive inn et nytt navn.
     */
    public void addPatientPushButtonPushed(ActionEvent actionEvent) {
        Patient newPatient = new Patient(firstNameTextField.getText(),
                                            lastNameTextField.getText(),
                                            socialSecurityNumberTextField.getText());
        //Get all patients from table and add new
        tableView.getItems().add(newPatient);

        firstNameTextField.clear();
        lastNameTextField.clear();
        socialSecurityNumberTextField.clear();
    }

    /**
     *
     * @param event
     * @throws Exception
     *
     * Denne metoden var laget for å få opp "PopUp.fxml". Har prøvd uttallige metoder
     * for å få den til å fungere, men det virker som at det er en feil i javafx.
     */
    public void addPatientPopUp(ActionEvent event) throws Exception {
        try {
            URL url = new File("C:\\Users\\Aleksander\\Skrivebord\\PatientApplication\\src\\main\\resources\\no\\ntnu\\idatt2001\\PopUp.fxml").toURI().toURL();
            Parent root = (Parent) FXMLLoader.load(url);

            Stage stage = new Stage();
            stage.setTitle("Add patient");
            stage.setScene(new Scene(root));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    PatientRegister patientRegister = new PatientRegister();


    /**
     * Dette er metoden for å lese csv fil. Fordi jeg ikke får opp andre vinduer så fungerer ikke denne.
     * Jeg vill ha lagt det opp slik jeg leste "filepathen" jeg fikk fra brukeren og lagt den inn i metoden.
     * Da skulle denne metoden fungert som den skulle.
     *
     * Metoden leser gjennom CSV filen og deler etter komma og linjer. Den bruker deretter
     * getOnePatient metoden for å hente verdiene som et objekt. Deretter legger den
     * objektene inn i tableview.
     *
     */
    /*
    public void addPatientsFromCSVFile(String file) {
        List<Patient> custPatients = new ArrayList<>();
        Path pathToFile = Paths.get(file);

        try (BufferedReader br = Files.newBufferedReader(pathToFile)) {
            String row = br.readLine();
            while (row != null) {
                String[] values = row.split(",");
                Patient patient = getOnePatient(values);
                custPatients.add(patient);
                row = br.readLine();
            }
            for(Patient p : custPatients) {
                String fName = p.getFirstName();
                String lName = p.getLastName();
                String sSN = p.getSocialSecurityNumber();

                Patient newPatient = new Patient(fName, lName, sSN);

                tableView.getItems().add(newPatient);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //Dette er getOnePatient metoden som tar inn en liste og gjør den om til et Patient objekt.

    private static Patient getOnePatient(String[] values) {
        String custFName = values[0];
        String custLName = values[1];
        String custSSnumber = values[2];

        return new Patient(custFName, custLName, custSSnumber);
    }


     */



    //Prøvde her å lage en metode for å kunne redigere pasienter. Fikk ikke tid til å
    //se mer på denne, og vet at den ikke fungerer.

    /*
    public void changeFirstNameCellEvent(CellEditEvent edditedCell) {
        Patient patientSelected = tableView.getSelectionModel().getSelectedItem();
        patientSelected.setFirstName(edditedCell.getNewValue().toString());
    }

    public void changeLastNameCellEvent(CellEditEvent edditedCell) {
        Patient patientSelected = tableView.getSelectionModel().getSelectedItem();
        patientSelected.setLastName(edditedCell.getNewValue().toString());
    }

    public void changeSocialSecurityNumberCellEvent(TableColumn.CellEditEvent edditedCell) {
        Patient patientSelected = tableView.getSelectionModel().getSelectedItem();
        patientSelected.setSocialSecurityNumber(edditedCell.getNewValue().toString());
    }
     */

    /**
     *
     * @param url
     * @param resourceBundle
     *
     * Setter her referansen til de forskjellige kolonnene.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {


        firstnameColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("lastName"));
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("socialSecurityNumber"));
    }


}

