package no.ntnu.idatt2001.PatientApplication;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class PatientApplicationMain extends Application {


    /**
     *
     * @param primaryStage
     * @throws IOException
     *
     * Åpner hovedvinduet i applikasjonen.
     */
    @Override
    public void start(Stage primaryStage) throws IOException {
        try {
            URL url = new File("C:\\Users\\Aleksander\\Skrivebord\\PatientApplication\\src\\main\\resources\\no\\ntnu\\idatt2001\\MainWindow.fxml").toURI().toURL();
            Parent root = FXMLLoader.load(url);
            primaryStage.setTitle("Main window");
            primaryStage.setScene(new Scene(root));
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static void main(String[] args) {
        launch(args);
    }
}
