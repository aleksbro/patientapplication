package no.ntnu.idatt2001.PatientApplication.patients;

import java.io.Serializable;
import java.util.Objects;

public class Patient implements Serializable {
    private static final long serialVersionUTD = 1L;

    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;


    public Patient() {

    }

    /**
     *
     * @param socialSecurityNumber
     * @param firstName
     * @param lastName
     * @param diagnosis
     * @param generalPractitioner
     *
     * Dette er en konstruktør. Slik som programmet er satt opp nå vil den aldri bli kalt på
     * fordi det ikke er mulig å legge inn diagnosis og general practitioner.
     */
    public Patient(String socialSecurityNumber, String firstName, String lastName, String diagnosis, String generalPractitioner) {
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    /**
     *
     * @param socialSecurityNumber
     * @param firstName
     * @param lastName
     * @param generalPractitioner
     *
     * Dette er en konstruktør. Slik som programmet er satt opp nå vil den aldri bli kalt på
     *      * fordi det ikke er mulig å legge inn general practitioner.
     */
    public Patient(String socialSecurityNumber, String firstName, String lastName, String generalPractitioner) {
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = "This patient has not been diagnosed.";
        this.generalPractitioner = generalPractitioner;
    }


    /**
     *
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     *
     * Dette er en konstruktør og er den som blir i hovedsak brukt. Her sjekkes også at
     * verdiene ikke er null, da dette ikke er noe grunn til å kunne gjøre.
     * Planen var å lage et pop up vindu når denne feilkoden blir sendt.
     * Da dette ikke var mulig, så blir exception sendt i terminal.
     * Her ville jeg seinere lagt inn en sjekk på at social security number er
     * minst 11 nummer langt.
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber) {

        if(firstName == null || firstName.isEmpty()) {
            throw new IllegalArgumentException("You must write a name!");
        } else {
            this.firstName = firstName;
        }

        if (lastName == null || lastName.isEmpty()) {
            throw new IllegalArgumentException("You must write a last name!");
        } else {
            this.lastName = lastName;
        }

        if(socialSecurityNumber == null || socialSecurityNumber.isEmpty()) {
            throw new IllegalArgumentException("The social security number can't be blanc.");
        } else {
            this.socialSecurityNumber = socialSecurityNumber;
        }



    }

    /**
     * Velger å ha get og set metoder på alle, da individuelle feil fort kan oppstå.
     * Det blir da unødvendig vanskelig å ikke kunne endre på dette.
     * @return String
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;
        Patient patient = (Patient) o;
        return Objects.equals(getSocialSecurityNumber(), patient.getSocialSecurityNumber()) && Objects.equals(getFirstName(), patient.getFirstName()) && Objects.equals(getLastName(), patient.getLastName()) && Objects.equals(getDiagnosis(), patient.getDiagnosis()) && Objects.equals(getGeneralPractitioner(), patient.getGeneralPractitioner());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSocialSecurityNumber(), getFirstName(), getLastName(), getDiagnosis(), getGeneralPractitioner());
    }

    /**
     *
     * @return String
     *
     * Dette er en toString som er bygd opp slik at man kan skrive til CSV fil.
     */
    @Override
    public String toString() {
        return new StringBuffer().append(this.firstName).append(", ")
                .append(this.lastName).append(", ")
                .append(this.socialSecurityNumber).append(", ").append("\n").toString();
    }
}
