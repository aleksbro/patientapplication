package no.ntnu.idatt2001.PatientApplication.patients;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class PatientRegister {

    /**
     * Dette er en liste som kan inneholde pasienter. Denne blir litt meningsløs
     * da man skal lagre til csv fil.
     */
    private final ArrayList<Patient> patients;

    public PatientRegister() {
        this.patients = new ArrayList<>();
    }






    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     * Denne metoden returnerer en observable list.Denne er enklere å bruke
     * sammen med tableview.
     */
    public ObservableList<Patient> obsPatients() {
        ObservableList<Patient> patientsClone = FXCollections.observableArrayList();
        patientsClone.addAll(getPatients());

        return patientsClone;
    }

    /**
     *
     * @param patient
     * Metode for å adde pasient. Blir litt overflødig med tanke på måten pasienter skal lagres.
     */
    public void addPatient(Patient patient) {
        if(patients.contains(patient)) {
            throw new IllegalArgumentException("This patient already exists.");
        }
        patients.add(patient);
    }

    /**
     *
     * @param patient
     * @return
     * @throws Exception
     *
     * Metode for å fjerne pasient. Blir også litt øverflødig pga array ikke brukes.
     */
    public boolean removePatient(Patient patient) throws Exception {
        boolean removed = false;

        if(!(patients.contains(patient))) {
            throw new Exception("This patient does not exist.");
        } else {
            patients.remove(patient);
            removed = true;
        }

        return removed;
    }

    /**
     *
     * Prøvde først å skrive til fil. Disse skal ikke brukes i denne applikasjonen, men lot
     * de stå her likevel. De kan fjernes
     */
    /*
    public void savePatientInFile(Object object) {
        String filePath = "C:\\Users\\Aleksander\\Skrivebord\\PatientApplication\\src\\main\\resources\\no\\ntnu\\idatt2001\\PatientSaver";
        try{
            FileOutputStream fileOut = new FileOutputStream(filePath);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(object);
            objectOut.close();
            System.out.println("The object was written to file");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void savePatientsInFile(ArrayList<Patient> list) {

        String filePath = "C:\\Users\\Aleksander\\Skrivebord\\PatientApplication\\src\\main\\resources\\no\\ntnu\\idatt2001\\PatientSaver";

        try{
            FileOutputStream fileOut = new FileOutputStream(filePath);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);

            for (Patient p : list) {
                objectOut.writeObject(p.toString());
            }

            objectOut.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

     */
}
