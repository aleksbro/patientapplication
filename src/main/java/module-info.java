/*
module no.ntnu.idatt2001.PatientApplication {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.rmi;
    requires java.logging;
    exports no.ntnu.idatt2001.PatientApplication;
    opens no.ntnu.idatt2001.PatientApplication to javafx.fxml;
    opens no.ntnu.idatt2001 to javafx.fxml, javafx.graphics;
}

 */
module no.ntnu.idatt.PatientApplication {
        requires javafx.controls;
        requires javafx.fxml;
        exports no.ntnu.idatt2001.PatientApplication;
        exports no.ntnu.idatt2001.PatientApplication.patients;
        opens no.ntnu.idatt2001.PatientApplication to javafx.fxml;
        opens no.ntnu.idatt2001.PatientApplication.patients to javafx.fxml;
        }